#!/usr/bin/env python3

import yaml
import os

conf_file = "conf.yml"

if __name__ == "__main__":

    with open(conf_file, 'r') as stream:
        serial_cfg = yaml.safe_load(stream)

    try:
        print( "'device': Device is a string", str(serial_cfg['device'] ) )
        if not os.path.dirname(serial_cfg['device'])=='/dev':
            raise( ValueError( 'Device is not in /dev' ) )
        print( "'device': Device is located in /dev:", os.path.dirname(serial_cfg['device'])=='/dev' ) 
        print( "'baud': Baud rate is an integer and is", int(serial_cfg['baud']) )

    except Exception as ex:
        print( "Config file error")
        print( "Exception was: ", ex )
        exit(1)

    exit(0)

